filetype off
filetype plugin indent on

" Gets rid of vi compatible stuff 
set nocompatible

" Gets rid of modelines for security purposes
set modelines=0

" Set tab settings
set expandtab
set ts=2
set sts=2
set sw=2
autocmd Filetype html setlocal ts=2 sts=2 sw=2 expandtab
autocmd Filetype ruby setlocal ts=2 sts=2 sw=2 expandtab
autocmd Filetype javascript setlocal ts=4 sts=4 sw=4 noexpandtab

" Organizing swap files (http://vi.stackexchange.com/a/179)
set directory^=$HOME/.vim/tmp//

" All of the following are from Steve Losh's Vim setup, with slight
" modifications
" http://stevelosh.com/blog/2010/09/coming-home-to-vim/
set encoding=utf-8
set scrolloff=3
set autoindent
set showmode
set showcmd
set hidden
set wildmenu
set wildmode=list:longest
set visualbell
"set cursorline
set ttyfast
set ruler
set backspace=indent,eol,start
set laststatus=2
set number
"set relativenumber 
set undodir=~/.vim/undo-dir
set undofile
set nofoldenable    " disable code folding
set foldmethod=indent

" Setting up basic remaps
inoremap jj <ESC>
let mapleader = ","
syntax on

" NERDTree configuration
nnoremap <leader>n :NERDTreeToggle<cr>

" Searching
nnoremap / /\v
vnoremap / /\v
set ignorecase
set smartcase
set gdefault
set incsearch
set showmatch
set hlsearch
nnoremap <leader><space> :noh<cr>
nnoremap <leader>a :Ack!<Space>
nnoremap <tab> %
vnoremap <tab> %
nnoremap <leader>t :CtrlPMixed<CR>

" Line handling
set textwidth=79
set wrap
set formatoptions=qrn1
set colorcolumn=85

" Disabling arrow keys
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>
nnoremap j gj
nnoremap k gk


" Leaders
nnoremap <leader>w <C-w><C-v><C-w>l
nnoremap <leader>q <C-w>q
" Trims whitespace
nnoremap <leader>W :%s/\s\+$//<cr>:let @/=''<CR>


" Color Schemes
set background=dark
colorscheme zenburn

" Window management
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Change behavior of enter key
map <Enter> o<ESC>
map <S-Enter> O<ESC>

" Ignore settings
" set wildignore+=*/out/**
" set wildignore+=*/build/**
set wildignore+=*/.git/**

" Clipboard management
set clipboard=unnamed

" JUST VIM:
" All the following are from: https://youtu.be/XA2WjJbmmoM:

" Sets the current path to search recursively in subdirectories
set path+=**

" TODO: set current file path with 'echo expand("%")'
" TODO: figure out how to use tmux with VIM
